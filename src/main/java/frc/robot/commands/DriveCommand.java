// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.ExampleSubsystem;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

/** An example command that uses an example subsystem. */
public class DriveCommand extends CommandBase {

  public WPI_TalonFX topLeftMotor;
  public WPI_TalonFX midLeftMotor;
  public WPI_TalonFX bottomLeftMotor;
  public WPI_TalonFX topRightMotor;
  public WPI_TalonFX midRightMotor;
  public WPI_TalonFX bottomRightMotor;

  public SpeedControllerGroup leftMotors;
  public SpeedControllerGroup rightMotors;

  public DifferentialDrive drive;

  public XboxController xboxDrive;

  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final Drivetrain m_subsystem;

  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public DriveCommand(Drivetrain subsystem) {
    m_subsystem = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    topLeftMotor = new WPI_TalonFX(11);
    midLeftMotor = new WPI_TalonFX(12);
    bottomLeftMotor = new WPI_TalonFX(13);
    topRightMotor = new WPI_TalonFX(14);
    midRightMotor = new WPI_TalonFX(15);
    bottomRightMotor = new WPI_TalonFX(16);
    topLeftMotor.setNeutralMode(NeutralMode.Brake);
    midLeftMotor.setNeutralMode(NeutralMode.Brake);
    bottomLeftMotor.setNeutralMode(NeutralMode.Brake);
    topRightMotor.setNeutralMode(NeutralMode.Brake);
    midRightMotor.setNeutralMode(NeutralMode.Brake);
    bottomRightMotor.setNeutralMode(NeutralMode.Brake);
    // leftMotors = new SpeedControllerGroup(topLeftMotor,midLeftMotor,bottomLeftMotor);
    // rightMotors = new SpeedControllerGroup(topRightMotor,midRightMotor,bottomRightMotor);
    // drive = new DifferentialDrive(leftMotors, rightMotors);
    xboxDrive = new XboxController(3);
  }

  public void drive(float speed) {
    topLeftMotor.set(-speed);
    midLeftMotor.set(-speed);
    bottomLeftMotor.set(-speed);
    topRightMotor.set(speed);
    midRightMotor.set(speed);
    bottomRightMotor.set(speed);
  }

  public void turn(float speed) {
    topLeftMotor.set(speed);
    midLeftMotor.set(speed);
    bottomLeftMotor.set(speed);
    topRightMotor.set(speed);
    midRightMotor.set(speed);
    bottomRightMotor.set(speed);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (xboxDrive.getY(Hand.kLeft) >= 0.2 || xboxDrive.getY(Hand.kLeft) <= -0.2) {
      drive((float)xboxDrive.getY(Hand.kLeft)*0.9f);
    } else {
      turn((float)xboxDrive.getX(Hand.kLeft)*0.9f);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
